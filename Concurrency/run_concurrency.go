package main

import (
	"errors"
	"fmt"
	"sync"
)

type workFunc func(val int) int

func RunConcurrency(concurrencyProcesses int, jobList []int, work workFunc) ([]int, error) {

	var wg sync.WaitGroup
	// 讓寫error時不衝撞
	var lock sync.Mutex
	wg.Add(concurrencyProcesses)

	// 預設兩倍量
	queue := make(chan int, concurrencyProcesses*2)
	found := make(chan int)

	errMap := make(map[int]error, 0)

	// 先將執行數列塞進通道
	go func(queue chan int) {
		for _, v := range jobList {
			queue <- v
		}
		close(queue)
	}(queue)

	for i := 0; i < concurrencyProcesses; i++ {
		go func(queue chan int, found chan int) {
			defer wg.Done()
			for val := range queue {
				var err error
				//err = errors.New("test error")
				if err != nil {
					lock.Lock()
					errMap[val] = err
					lock.Unlock()
				}
				res := work(val)
				found <- res
			}
		}(queue, found)
	}

	go func() {
		wg.Wait()
		close(found)
	}()

	var results []int
	finishChan := make(chan []int, 1)
	go func() {
		defer close(finishChan)
		for p := range found {
			fmt.Println("Finished job:", p)
			results = append(results, p)
		}
		finishChan <- results
	}()

	select {
	case result := <-finishChan:
		errStr := ""
		for _, v := range errMap {
			errStr += v.Error() + ","
		}
		if errStr != "" {
			return result, errors.New(errStr)
		}

		return result, nil
	}
}
