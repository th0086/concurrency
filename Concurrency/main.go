package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {

	// 設定100個數的工作要跑
	var jobIntList []int
	for i := 0; i < 100; i++ {
		jobIntList = append(jobIntList, i)
	}

	// 執行包裝過的Concurrency，10個工作線、執行數列、執行工作
	resultsInt, err := RunConcurrency(10, jobIntList, WorkInt)
	if err != nil {
		fmt.Println(err)
		return

	}
	
	fmt.Println("result:", resultsInt)
}

// 隨機執行時間

func WorkInt(val int) int {
	waitTime := rand.Int31n(1000)
	fmt.Println("job:", val, "wait time:", waitTime, "millisecond")
	time.Sleep(time.Duration(waitTime) * time.Millisecond)
	return val
}
